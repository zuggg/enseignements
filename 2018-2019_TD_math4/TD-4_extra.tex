%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{TD 4, extra -- Transformée de Laplace}
\author{}
\date{}

%%------------------------------------------------------------------------------
%% Packages

\usepackage{amsmath,amsfonts}
%% \usepackage{amssymb}
\usepackage{mathtools}
\usepackage[matha,mathb]{mathabx} % french mathematical symbols and more
%% \usepackage[integrals]{wasysym} % german integrals
\usepackage{amsthm}
\usepackage[]{geometry}
\usepackage[svgnames]{xcolor}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{enumitem}

\usepackage{hyperref}
\hypersetup{colorlinks=true,
  citecolor=DarkBlue,
  linkcolor=DarkBlue,
  linktoc=page,
  urlcolor=black,
}

\usepackage[french]{babel}


%%------------------------------------------------------------------------------
%% Custom environments/macros

\renewcommand{\thesection}{\Alph{section}}

\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\N}{\mathbb{N}}

\newcommand{\mc}{\mathcal}

\DeclarePairedDelimiter{\norm}{\lVert}{\rVert} %% requires mathtools
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\sbra}{[}{]}
\DeclarePairedDelimiter{\pare}{(}{)}
\DeclarePairedDelimiter{\osbral}{]}{}
\DeclarePairedDelimiter{\osbra}{]}{[}
\DeclarePairedDelimiter{\osbrar}{[}{[}


\theoremstyle{definition}
\newtheorem*{defi}{Définition}
%% \newtheorem{qstn}{}
%% \newtheorem{subqstn}{}[qstn]


\newcounter{question}[section]
\newcounter{subquestion}[question]

\newcommand{\qstn}{\stepcounter{question}\paragraph{\thequestion.}~}
\newcommand{\sqstn}{\stepcounter{subquestion}\paragraph{\thequestion.\thesubquestion.}~}
%\newcommand{\sqstn}{\stepcounter{subquestion}\newline\noindent\textbf{\thequestion.\thesubquestion.}\hspace{1em}}


%%------------------------------------------------------------------------------
%% fancyhdr

\pagestyle{fancy}
\fancyhf{}
\rhead{PCSI L2, Mathématiques 4, 2018--2019}
\lhead{Transformation de Laplace}
%\fancyfoot[EL,OR]{\thepage /\pageref{LastPage}}
\rfoot{\thepage /\pageref{LastPage}}
\renewcommand{\footrulewidth}{0.5pt} %Filet en bas de page

\fancypagestyle{plain}{%
  \fancyhf{} % clear all header and footer fields
  \rfoot{\thepage /\pageref{LastPage}}
  \renewcommand{\headrulewidth}{0pt}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\thispagestyle{plain}
\begin{center}
  {\LARGE \bf TD 4 Extra -- Transformation de Laplace}
\end{center}
\bigskip

\emph{Ce sujet est largement issu du sujet de l'épreuve du concours Centrale Maths 1, PSI, en 2012, qui est consultable sur le site de \href{https://www.prepamag.fr/concours/dl.html?q=PSI_MATHS_CENTRALE_1_2012}{l'édition H\&K}.} 

\section{Préliminaires et définitions}

On rappelle que \(\mc C^0(\R_+,\R)\) désigne l'ensemble des fonctions définies sur \(\R_+\), continues et à valeur dans \(\R\). On notera aussi cet ensemble simplement \(\mc C^0\) lorsque il n'y a pas d'ambiguïté sur le domaine de définition et l'ensemble d'arrivée. De la même manière, \(C^1\) désigne l'ensemble des fonctions dérivables et de dérivée continue.

Dans tout le problème, on considère une fonction \(\lambda\), continue et de \(\R_+\) dans \(\R\), croissante et non majorée. 
\begin{defi}
  Soit \(f:\R\to\R\) une fonction continue. On note
  \begin{itemize}
  \item \(E_f\) l'ensemble des \(x\in\R\) tels que \(t\mapsto f(t)e^{-x\lambda(t)}\) est intégrable sur \(\R_+\),
  \item \(E'_f\) l'ensemble des \(x\in\R\) tels que \(\int_0^{+\infty}f(t)e^{-x\lambda(t)}dt\) converge,
  \item et, pour \(x\in E_f\),
  \[
  Lf(x)=\int_0^{+\infty}f(t)e^{-x\lambda(t)}dt.
  \]
  \end{itemize}
\end{defi}
On se propose ci-après d’étudier la transformation \(f \mapsto Lf\), d’en établir quelques propriétés, d’examiner certains exemples et d’utiliser la transformation \(L\) pour l’étude d’un opérateur



\qstn Quelle inclusion existe-t-il entre \(E_f\) et \(E'_f\) ?

\qstn On suppose dans cette question que \(E_f\) n'est pas vide.
\sqstn Montrer que dans ce cas \(E_f\) est un intervalle non majoré de \(\R\).
\sqstn Montrer qu'alors, \(Lf\) est continue sur \(E_f\).
\sqstn Finalement, en supposant de plus que \(\lambda(t)>0\) pour tout \(t>0\), montrer que \(Lf\) est décroissante sur \(E_f\), et que \(\lim_{x\to+\infty}Lf(x)=0\).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Exemples dans le cas où \texorpdfstring{\boldmath\(f\)}{f} est positive}

\qstn Comparer \(E_f\) et \(E'_f\) dans le cas où \(f\) est positive

\qstn Dans les trois cas suivants, déterminer \(E_f\) :
\begin{enumerate}[label=(\roman*)]
\item \(f=\lambda'\), où on suppose que \(\lambda\) est dérivable, et de dérivée continue.
\item \(f:t\mapsto e^{t\lambda(t)}\).
\item \(\displaystyle f:t\mapsto \frac{e^{-t\lambda(t)}}{1+t^2}.\)
\end{enumerate}

\qstn Dans cette question uniquement, on étudie le cas où, pour tout \(t\in\R_+\),
\[
\lambda(t)=t^2,\quad f(t)=\frac{1}{1+t^2}.
\]
\sqstn Déterminer \(E_f\). Que vaut \(Lf(0)\) ?
\sqstn Soit \(a>0\). Montrer que \(Lf\) est dérivable sur \(\osbra*{a,+\infty}\). En déduire que \(Lf\) est dans \(\mc C^1(\osbra*{0,+\infty},\R)\), et expliciter sa dérivée.
\sqstn Montrer que pour tout \(x>0\), on a
\[
Lf(x)-(Lf)'(x) = \frac{A}{\sqrt x},\quad \text{où }A=\int_0^{+\infty}e^{-t^2}dt.
\]
\sqstn Soit maintenant, pour \(x\geq0\), \(g(x)=e^{-x}Lf(x)\). En dérivant \(g\), montrer que
\[
g(x)=\frac{\pi}{2}-A\int_0^x\frac{e^{-t}}{\sqrt t}dt.
\]
\sqstn En utilisant le résultat de la question 2.3, en conclure que \(\displaystyle\int_0^{+\infty} e^{-t^2}dt=\frac{\sqrt \pi}{2}\).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Étude en la borne inférieure de \texorpdfstring{\boldmath\(E_f\)}{Ef}}

On suppose que \(f\) est positive et que \(E_f\) n’est ni vide ni égal à \(\R\). On note \(\alpha\) sa borne inférieure, c'est-à-dire que \(E_f=\osbra{\alpha,+\infty}\) ou \(E_f=\osbrar{\alpha,+\infty}\).

\qstn On suppose dans cette question que \(Lf\) est bornée sur \(E_f\) : il existe \(M>0\) tel que \(\forall\,x\in E_f\), \(Lf(x)\leq M\).  On va montrer qu'alors, \(\alpha\in E_f\).
\sqstn Soit \(A>0\). Montrer que la fonction
\[
L_Af(x)=\int_0^A f(t)e^{-x\lambda(t)}dt
\]
est bien définie sur \(\R\) et y est continue.
\sqstn Montrer que pour tout \(x\in E_f\), \(L_Af(x)\leq M\), et en déduire que \(L_Af(\alpha)\leq M\).
\sqstn Conclure.

\qstn On suppose maintenant que \(\lambda(t)\geq 0\) pour tout \(t>0\), et que \(\alpha\not\in E_f\). Que peut-on dire sur \(Lf(x)\) lorsque \(x\) tend vers \(\alpha\) par valeurs supérieures ?

\qstn Peut-on arriver à la même conclusion en ne supposant pas que \(\lambda\geq 0\) ?

%%  VII.A – Cas positif
%% On suppose que f est positive et que E n’est ni vide ni égal à R. On note α sa borne inférieure.
%% VII.A.1) Montrer que si Lf est bornée sur E, alors α ∈ E.
%% VII.A.2) Si α ∈/ E, que dire de Lf(x) quand x tend vers α
%% + ?
%% VII.B – Dans cette question, f(t) = cost et λ(t) = ln(1 + t).
%% VII.B.1) Déterminer E.
%% VII.B.2) Déterminer E′
%% .
%% VII.B.3) Montrer que Lf admet une limite en α, borne inférieure de E et la déterminer.
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
