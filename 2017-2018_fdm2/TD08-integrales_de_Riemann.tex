%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Fondamentaux des Maths 2 -2018 - Feuille de TD8
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\title{}
\author{Simon Zugmeyer}
\date{}

%%------------------------------------------------------------------------------
%% Packages

\usepackage{amsmath,amsfonts}
\let\amsast=\ast
\usepackage{mathtools}
\usepackage[matha,mathb]{mathabx} % french mathematical symbols and more
%% \usepackage[integrals]{wasysym} % german integrals
\usepackage{amsthm}
\usepackage[]{geometry}
\usepackage{multicol}
\usepackage{enumitem}
%% \usepackage[svgnames]{xcolor}
%% \usepackage{asymptote} % graphs!
\usepackage{adjustbox} 
\usepackage{tasks} % horizontal enumerates, better control over spacing
\usepackage{version} % hide corrections

\usepackage[french]{babel}

%%------------------------------------------------------------------------------
%% Custom environments/macros

%% \renewcommand{\theenumi}{\roman{enumi}} % roman numerals in enumerate

\DeclarePairedDelimiter{\norm}{\lVert}{\rVert} %% requires mathtools
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}

\makeatletter
% boldmath in titles
\g@addto@macro\bfseries{\boldmath}
% straight d in integrals
\renewcommand\d[1]{\mspace{2mu}\mathrm{d}#1\@ifnextchar\d{\mspace{-1mu}}{}}
% Swap the definition of \abs* and \norm*, so that \abs
% and \norm resizes the size of the brackets, and the 
% starred version does not.
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
%
\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother


%% Theorems
\theoremstyle{definition}
\newtheorem{exo}{Exercice}

\theoremstyle{remark}
\newtheorem*{cor}{Correction}

%% Math shortcuts
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\N}{\mathbb{N}}

\newcommand{\eps}{\varepsilon}
\newcommand{\mc}{\mathcal}

%%-----------------------------------------------------------------------
%% enumitem, tasks

\settasks{item-indent=20pt,style=enumerate,before-skip=5pt,after-skip=5pt}
\setlist[enumerate]{leftmargin=20pt}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% version package, comment to print solutions to exercises
%% uncomment to hide them.
%% labels may change, so you might have to compile twice

 \excludeversion{cor}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\noindent 
Universit\'e Claude Bernard Lyon 1  
\hfill UE Fondamentaux des Math\'ematiques II\\
Semestre de printemps 2017--2018
\bigskip 


\begin{center}
{\Large\bf Feuille 8 : Intégrales de Riemann} 
\end{center}
\bigskip

\section{Calcul d'aires}

\begin{exo}
  Considérons
  \begin{align*}
    f:\left[-1,1\right]&\to \R\\
    x&\mapsto \sqrt{1-x^2}.
  \end{align*}
  \begin{enumerate}
  \item Dessiner le graphe de $f$.
  \item Soit $x\in\left[-1,1\right]$. Montrer que si l'on pose $\theta=\arccos(x)$, alors ${(x,f(x))=(\cos\theta,\sin\theta)}$.
  \item En déduire $\int_{-1}^1f(x) \d x$.
  \end{enumerate}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item \adjustbox{valign=t}{%
    \includegraphics{TD08-integrales_de_Riemann-3}
  }
  \item $x\in\left[-1,1\right]$, et donc $x=\cos(\arccos(x))=\cos\theta$. Mais alors $f(x)=\sqrt{1-\cos^2\theta}=\abs{\sin\theta}=\sin\theta$, puisque $\theta\in\left[0,\pi\right]$.
  \item D'après la question précédente, le graphe de $f$ décrit exactement un demi-cercle de centre $0$ et de rayon $1$. On en déduit que $\int_{-1}^1f(x)\d x=\pi/2$.
  \end{enumerate}
\end{cor}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{exo}
  Déterminer l'aire des domaine hachurés représentés ci-dessous.

  \begin{multicols}{2}
    \begin{enumerate}
    \item
      \adjustbox{valign=t}{%
	\includegraphics{TD08-integrales_de_Riemann-1}
      }
    \item
      \adjustbox{valign=t}{%
    	\includegraphics{TD08-integrales_de_Riemann-2}
      }
    \end{enumerate}
  \end{multicols}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item Les courbes d'équation $y=1$ et $y=x^2$ se croisent en les points $(-1,1)$ et $(1,1)$. Ainsi, l'aire du domaine, que l'on note $A$, est donnée par
    $$
    A=\int_{-1}^1 1\d x -\int_{-1}^1 x^2\d x=2-\frac{2}{3}=\frac{4}{3}
    $$
  \item Cette fois-ci, on intègre entre $0$ et $2$, mais il faut faire attention, car les courbes se croisent, et il faut donc séparer l'intégrale en deux morceaux. Ainsi, l'aire est 
    \begin{align*}
      A=\int_0^1 (\sqrt x-x)\d x +\int_1^2 (x-\sqrt x)\d x&= \left[\frac{2}{3}x^{3/2}-\frac{1}{2}x^2\right]_0^1+\left[\frac{1}{2}x^2-\frac{2}{3}x^{3/2}\right]_1^2\\
      &=\frac{2}{3}-\frac{1}{2}+2-\frac{2}{3}2^{3/2}+\frac{2}{3}-\frac{1}{2}\\
      &=\frac{1}{3}(7-4\sqrt2)
    \end{align*}
    
  \end{enumerate}
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{exo}
  Déterminer sans aucun calcul la valeur de l'intégrale
  $$
  \int_{-\pi}^\pi \sin(x)\sqrt{3+x^2}\d x.
  $$
  On pourra s'aider d'un dessin.
\end{exo}

\begin{cor}
  La fonction $x\mapsto \sin(x)\sqrt{3+x^2}$ est impaire, et donc son graphe présente une symétrie centrale par rapport à l'origine. Ainsi, l'aire algébrique sous son graphe sur le segment $\left[-\pi,0\right]$ est exactement l'opposé de l'aire sous son graphe sur le segment $\left[0,\pi\right]$, ce qui montre bien que son intégrale sur la réunion de ses segments est nulle.
  \begin{figure}[h]
    \centering
    \includegraphics{TD08-integrales_de_Riemann-4}
  \end{figure}
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{exo}
  On considère la fonction
  \begin{align*}
    f:\left[0,1\right]&\to \R\\
    x&\mapsto x.
  \end{align*}
  \begin{enumerate}
  \item Sans faire de calcul, et grâce à des considérations géométriques, montrer que
    $$
    \int_0^1f(x)\d x=1/2.
    $$
  \item On se propose de retrouver ce résultat en faisant appel à la définition de l'intégrale de Riemann.
    \begin{enumerate}
    \item Soit $n\in\N^*$. On définit les fonctions en escalier $u_n$ et $v_n$ sur $\left[0,1\right]$ par 
    $$
    \forall k\in\{0,\dots,n-1\},\, \forall x\in\left[\frac{k}{n},\frac{k+1}{n}\right[,\quad u_n(x)=f\left(\frac{k}{n}\right) \text{ et } v_n(x)=f\left(\frac{k+1}{n}\right),
	$$
	et $u_n(1)=v_n(1)=1$.
	Faire un dessin des fonctions $f$, $u_n$ et $v_n$. Montrer que, par définition de l'intégrale de Riemann,
	$$
	\frac{1}{n}\sum_{k=1}^n \frac{k}{n}\leq \int_0^1 f(x)\d x \leq \frac{1}{n}\sum_{k=1}^n \frac{k+1}{n}.
	$$
      \item En faisant tendre $n$ vers $+\infty$, en conclure que $\int_0^1f(x)\d x=1/2$
    \end{enumerate}
  \item Finalement, retrouver le résultat encore une fois en faisant appel au théorème fondamental de l'analyse.
  \item Répéter les questions 2. et 3. avec, à la place de la fonction $f$, la fonction
    $$
    g:x\mapsto x^2.
    $$
  \end{enumerate}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item Le graphe de $x\mapsto x$ sur $\left[0,1\right]$ définit un triangle de base $1$ et de hauteur $1$, dont l'aire est l'intégrale de cette fonction, qui est donc égale à $1/2$.
  \item
    \begin{enumerate}
    \item
      On remarque que $u_n$ et $v_n$  vérifient $u_n(x)\leq f(x)\leq v_n(x)$ pour tout $x\in\left[0,1\right]$. Alors, par définition de l'intégrale de Riemann,
      $$
      \int_0^1 u_n(x)\d x \leq \int_0^1 f(x)\d x\leq \int_0^1 v_n(x)\d x,
      $$
      et donc, par définition de l'intégrale pour les fonctions en escalier,
      $$
      \sum_{k=1}^n \frac{1}{n}f\left(\frac{k}{n}\right)\leq \int_0^1 f(x)\d x \leq \sum_{k=1}^n \frac{1}{n}f\left(\frac{k+1}{n}\right),
      $$
      d'où l'inégalité voulue.
      \begin{figure}[h]
	\centering
	\includegraphics{TD08-integrales_de_Riemann-6}
      \end{figure}
	    
    \item En utilisant le fait que $\sum_{k=1}^n k=\frac{n(n+1)}{2}$, on trouve que
      $$
      \frac{n+1}{2n} \leq \int_0^1 f(x)\d x \leq \frac{n+1}{2n}+\frac{1}{n}.
      $$
      Ce résultat étant vrai pour tout $n\in\N^*$, on retrouve que $\int_0^1 f(x)\d x=1/2$ en faisant tendre $n$ vers $+\infty$.
    \end{enumerate}
  \item $f$ est continue, et $F:x\mapsto x^2/2$ est une primitive de $f$. Ainsi, d'après le théorème fondamental de l'analyse,
    $$
    \int_0^1 f(x)\d x= F(1)-F(0)=\frac{1}{2}.
    $$
  \item On procède à la même étude avec $g(x)=x^2$. Cette fois,
    $$
    \sum_{k=1}^n \frac{1}{n}g\left(\frac{k}{n}\right)\leq \int_0^1 g(x)\d x \leq \sum_{k=1}^n \frac{1}{n}g\left(\frac{k+1}{n}\right),
    $$
    donne
    $$
    \frac{1}{n^3}\sum_{k=1}^nk^2 \leq\int_0^1 g(x)\d x \leq \frac{1}{n^3}\sum_{k=1}^n(k+1)^2,
    $$
    d'où
    $$
    \frac{n(n+1)(2n+1)}{6n^3} \leq\int_0^1 g(x)\d x \leq \frac{(n+1)(n+2)(2n+3)}{6n^3}-\frac{1}{n^3}
    $$
    d'où, par passage à la limite, $\int_0^1 g(x)\d x=1/3$. La fonction $G(x)=x^3/3$ est une primitive de $g$, et utiliser le théorème fondamental de l'analyse amène au même résultat.
  \end{enumerate}
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Sommes de Riemann}

On rappelle le résultat suivant (dont on verra une preuve pour les fonctions de classe $\mc C^1$ dans l'exercice \ref{exo:erreur}) : si $f:[a,b]\mapsto\R$ est une fonction intégrable au sens de Riemann, alors 
$$
\frac{b-a}{n}\sum_{k=1}^nf\left(a+k\frac{b-a}{n}\right) \xrightarrow[n\to+\infty]{} \int_a^b f(x)\d x.
$$

\begin{exo}
  En utilisant ce résultat, calculer la limite des suites suivantes :
  \begin{tasks}(3)
    \task $\displaystyle u_n=\frac{1}{n}\sum_{k=1}^n\frac{1}{1+k/n},$
    \task $\displaystyle u_n=\sum_{k=1}^n\sqrt{\frac{k}{n^3}},$
    \task $\displaystyle u_n=\sum_{k=1}^n\frac{n}{k^2+n^2}.$
  \end{tasks}  
\end{exo}


\begin{cor}
  \begin{enumerate}
  \item $f:x\mapsto\frac{1}{1+x}$ est continue sur $\left[0,1\right]$, donc intégrable au sens de Riemann, et donc
    $$
    u_n=\frac{1}{n}\sum_{k=1}^nf\left(\frac{k}{n}\right) \xrightarrow[n\to+\infty]{} \int_0^1 \frac{1}{1+x}\d x= \ln(2).
    $$
  \item À nouveau, $x\mapsto\sqrt x$ est continue, donc
    $$
    u_n=\frac{1}{n}\sum_{k=1}^n\sqrt{\frac{k}{n}} \xrightarrow[n\to+\infty]{} \int_0^1\sqrt x\d x = \frac{3}{2}.
    $$
  \item Même argument pour montrer que
    $$
    u_n=\frac{1}{n}\sum_{k=1}^n\frac{1}{1+(k/n)^2} \xrightarrow[n\to+\infty]{} \int_0^1 \frac{1}{1+x^2}\d x=\arctan{1}=\frac{\pi}{4}.
    $$
  \end{enumerate}
\end{cor}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{exo}
  Déterminer la limite de la suite suivante :
  $$
  u_n=\sqrt[\uproot{2}n]{\frac{(2n)!}{n!n^n}}.
  $$
  \emph{Indication : on pourra étudier la suite $(\ln(u_n))$.}
\end{exo}

\begin{cor}
  On s'intéresse à la suite $v_n=\ln u_n$.
  \begin{align*}
    v_n=\ln u_n=\frac{1}{n}\ln\left(\frac{(2n)!}{n!n^n}\right)&=\frac{1}{n}\ln\left(\frac{(n+1)(n+2)\dots(2n)}{n^n}\right)\\
    &=\frac{1}{n}\sum_{k=1}^n\ln\left(\frac{n+k}{n}\right)\\
    &\xrightarrow[n\to+\infty]{}\int_1^2\ln(x)\d x\\
    &=\left[x\ln(x)-x\right]_1^2\\
    &=2\ln 2-1,
  \end{align*}
  donc $\lim_{n\to+\infty}u_n= 4/e$.
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{exo}
  On cherche à déterminer la limite de la suite $(u_n)_{n\in\N^\amsast}$ définie par
  $$
  u_n=\sum_{k=1}^n\sin\left(\frac{k}{n^2}\right)\sin\left(\frac{k}{n}\right).
  $$
  \begin{enumerate}
  \item Commençons par étudier la suite
    $$
    v_n=\sum_{k=1}^n\frac{k}{n^2}\sin\left(\frac{k}{n}\right).
    $$
    Montrer que la suite $(v_n)$ converge vers une limite à préciser. On pourra utiliser le fait que $x\mapsto \sin(x)-x\cos(x)$ est une primitive de $x\mapsto x\sin(x)$.
  \item Montrer que l'inégalité $x-x^3/6 \leq \sin(x) \leq x$ est vérifiée pour tout $x\geq0$.
  \item En déduire que pour tout $n\geq 1$,
    $$
    \abs{u_n-v_n}\leq\frac{1}{6n^2}.
    $$
  \item Conclure : établir que $\lim_{n\to+\infty}u_n=\sin(1)-\cos(1)$.
  \end{enumerate}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item $x\mapsto x\sin(x)$ est continue, et donc
    \begin{align*}
      v_n=\frac{1}{n}\sum_{k=1}^n\frac{k}{n}\sin\left(\frac{k}{n}\right)&\xrightarrow[n\to+\infty]{} \int_0^1x\sin(x)\d x\\
      &=\left[\sin(x)-x\cos(x)\right]_0^1\\
      &=\sin(1)-\cos(1).
    \end{align*}
  \item Considérons $f:x\mapsto\sin(x)-x+x^3/6$. En remarquant que $f^{(3)}(x)=-\cos(x)+1\geq0$, on montre que $f''$ est croissante. Alors, pour tout $x\geq 0$, $f''(x)=-\sin(x)+x\geq f''(0)=0$. On en déduit que pour tout $x\geq0$, $f'(x)\geq f'(0)=0$, et finalement que $\forall x\geq0,\,f(x)\geq0$. On a bien montré les deux inégalités demandées.
  \item L'inégalité de la question précédente implique que pour tout $x\geq 0$, on a $\abs{\sin(x)-x}\leq x^3/6$. Soit alors $n\in\N^*$, et $k\in\{1,\dots,n\}$. On trouve que
    $$
    \abs{\sin\left(\frac{k}{n^2}\right)-\frac{k}{n^2}}\leq\frac{k^3}{6n^6}\leq\frac{1}{6n^3}.
    $$
    Il vient alors, en sommant sur $k$,
    $$
    \abs{u_n-v_n}\leq n\frac{1}{6n^3}.
    $$
  \item On a montré que $\lim_{n\to+\infty}v_n=\sin(1)-\cos(1)$, et aussi que $\lim_{n\to+\infty}\abs{u_n-v_n}=0$, ce qui prouve bien que $\lim_{n\to+\infty}u_n=\sin(1)-\cos(1)$.
  \end{enumerate}
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Applications}


\begin{exo}[Comparaison série-intégrale]
  On se propose d'étudier la suite $(u_n)_{n\in\N^\amsast}$, définie par $u_n=\sum_{k=1}^n 1/k$.
  \begin{enumerate}
  \item Soit $k\in\N^*$. Montrer que
    $$
    \frac{1}{k+1} \leq \int_{k}^{k+1}\frac{1}{x}\d x \leq \frac{1}{k},
    $$
    par exemple avec un dessin, en graphant la fonction $x\mapsto1/x$.
  \item En déduire que pour tout $n\in\N^*$,
    $$
    \int_{1}^{n+1}\frac{1}{x}\d x\leq u_n \leq 1+\int_{1}^n\frac{1}{x}\d x.
    $$
  \item Que peut-on en conclure sur $\lim_{n\to+\infty} u_n$ ?
  \end{enumerate}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item La fonction $x\mapsto 1/x$ est décroissante sur $\R^*$, et donc, si $k\in\N^*$ et $x\in\left[k,k+1\right]$,
    $$
    \frac{1}{k+1} \leq \frac{1}{x} \leq \frac{1}{k}.
    $$
    Il suffit d'intégrer cette inégalité sur l'intervalle $\left[k,k+1\right]$ pour obtenir l'inégalité désirée. On peut aussi le voir graphiquement :
    \begin{figure}[h]
      \centering
      \includegraphics{TD08-integrales_de_Riemann-5}
    \end{figure}

  \item Soit $n\in\N^*$. En sommant l'inégalité précédente pour $k\in\{1,\dots,n\}$, on obtient
    $$
    u_{n+1}-1=\sum_{k=2}^{n+1}\frac{1}{k} \leq \int_1^{n+1}\frac{1}{x}\d x\leq \sum_{k=1}^{n}\frac{1}{k}=u_n,
    $$
    d'où, d'une part, $u_n\geq \int_1^{n+1}\frac{1}{x}\d x$, et d'autre part, $u_{n+1}\leq1+\int_1^{n+1}\frac{1}{x}\d x$, ce qui répond à la question.
  \item On a en particulier $u_n\geq\int_1^{n+1}\frac{1}{x}\d x=\ln(n+1)$, d'où $\lim_{n\to+\infty}u_n=+\infty$. On peut même aller un peu plus loin, et utiliser les deux inégalités pour montrer que $u_n\sim\ln(n)$ lorsque $n\to+\infty$.
  \end{enumerate}
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{exo}
  Soit $f:\left[a,b\right]\to\R_+$ une fonction continue telle que $\int_a^b f(x)\d x=0$. Montrer que pour tout $x\in\left[a,b\right]$, $f(x)=0$.
\end{exo}

\begin{cor}
  Notons déjà, comme $f\geq0$, que $\int_a^b f(x)\d x\geq0$. Prouvons alors la contraposée. Soit $f:\left[a,b\right]\to\R_+$ une fonction continue non identiquement nulle. Il existe $x_0\in\left[a,b\right]$ tel que $f(x_0)>0$. Alors, par continuité, il existe un réel $\eps$ tel que $b-a>\eps>0$, et, pour tout $x\in\left[x_0-\eps,x_0+\eps\right]\cap\left[a,b\right]$, $f(x)\geq f(x_0)/2$. Il vient donc, par croissance de l'intégrale, que
  $$
  \int_a^b f(x)\d x\geq \int_{\left[x_0-\eps,x_0+\eps\right]\cap\left[a,b\right]} f(x)\d x \geq \eps \frac{f(x_0)}{2}>0.
  $$
  En prenant la contreposée, on en déduit donc que $\int_a^b f(x)\d x=0 \implies \left(\forall x\left[a,b\right],\,f(x)=0\right)$.
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{exo}
  Soit $f:\left[0,1\right]\to\R$ une fonction continue. Montrer que
  $$
  \lim_{n\to+\infty}\int_0^1 x^nf(x)\d x= 0.
  $$
\end{exo}

\begin{cor}
  $f$ est continue sur un intervalle fermé, elle est donc bornée (et atteint ses bornes). Il existe donc $M>0$ tel que pour tout $x\in\left[0,1\right]$, on a $\abs{f(x)}\leq M$, et donc
  $$
  \abs{\int_0^1 x^nf(x)\d x}\leq \int_0^1 x^n\abs{f(x)}\d x \leq M\int_0^1 x^n\d x = M\left[\frac{x^{n+1}}{n+1}\right]_0^1 = \frac{M}{n+1},
  $$
  d'où le résultat.
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Un peu de théorie}

\begin{exo}[Une fonction non intégrable]
  Soit $f:\left[0,1\right]\mapsto\R$ la fonction définie par
  $$
  f(x)=
  \begin{cases}
    1 &\text{si }x\in\Q,\\
    0 &\text{si }x\in\R\backslash\Q.
  \end{cases}
  $$
  \begin{enumerate}
  \item Soit $g$ une fonction en escalier telle que $g\geq f$. Montrer que pour tout $x\in\left[0,1\right]$, $g(x)\geq 1$ (sauf éventuellement en ses points de discontinuité).
  \item De la même manière, montrer que si $h$ est une fonction en escalier $\leq f$, alors $h\leq0$ (sauf éventuellement en ses points de discontinuité).
  \item En déduire que
    \begin{align*}
      \inf\left\{\int_0^1g(x)\d x,\,g\text{ en escalier et }g\geq f\right\}=1, \\
      \sup\left\{\int_0^1h(x)\d x,\,h\text{ en escalier et }h\leq f\right\}=0.
    \end{align*}
  \item Conclure.
  \end{enumerate}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item $g$ étant en escalier, il existe une subdivision $a_0=0<a_1<\dots<a_n=1$ de $\left[0,1\right]$ telle que $g$ soit constante sur les intervalles de la forme $\left]a_{k-1},a_k\right[$, $k\in\{1,\dots,n\}$. Alors, comme $\Q$ est dense dans $\R$, il existe des rationnels $r_k\in\left]a_{k-1},a_k\right[$, et, par définition de $f$, $g(r_k)\geq f(r_k)=1$. Ainsi, $g$ est supérieure ou égale à $1$ sur les intervalles $\left]a_{k-1},a_k\right[$, $k\in\{1,\dots,n\}$, ce qui répond à la question.
      \item Le raisonnement est le même que dans la question $1.$, mais on utilise cette fois le fait que $\R\backslash\Q$ est dense dans $\R$.
      \item Si $g$ est une fonction en escalier $\geq f$, alors la question $1$ implique que $\int_0^1 g(x)\d x\geq 1$. On peut donc prendre la borne inférieure dans cette inégalité pour obtenir que
	$$
	\inf\left\{\int_0^1g(x)\d x,\,g\text{ en escalier et }g\geq f\right\}\geq 1.
	$$
	Comme de plus la fonction constante égale à $1$ est en escalier et est supérieure à $f$, on en déduit que $\inf\left\{\int_0^1g(x)\d x,\,g\text{ en escalier et }g\geq f\right\}= 1$. On montre l'autre inégalité de la même manière.
      \item Comme ces deux quantités sont différentes, on en conclut que $f$ n'est pas intégrable au sens de Riemann.
  \end{enumerate}
\end{cor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{exo}[Calcul d'erreur]\label{exo:erreur}
  Soit $f:\left[0,1\right]\mapsto\R$ une fonction de classe $\mc C^1$. Elle est continue, donc intégrable au sens de Riemann. On pose $S=\int_0^1 f(x)\d x$, et pour $n\geq1$,
  $$
  S_n=\frac{1}{n}\sum_{k=1}^nf\left(\frac{k}{n}\right).
  $$
  \begin{enumerate}
  \item Montrer que
    $$
    \abs{S-S_n}\leq\sum_{k=1}^n\int_{(k-1)/n}^{k/n}\abs{f(x)-f\left(\frac{k}{n}\right)}\d x.
    $$
  \item Si $k\in\{1,\dots,n\}$ et $x\in\left[\frac{k-1}{n},\frac{k}{n}\right]$, montrer à l'aide de l'inégalité des accroissements finis que
    $$
    \abs{f(x)-f\left(\frac{k}{n}\right)}\leq\frac{M}{n},
    $$
    où $M$ est une constante indépendante de $k$ et de $x$.
  \item En déduire que $\abs{S-S_n}\leq\frac{M}{n}$.
  \end{enumerate}
\end{exo}

\begin{cor}
  \begin{enumerate}
  \item La relation de Chasles permet d'écrire
    \begin{align*}
      \abs{S-S_n}&=\abs{\sum_{k=1}^n\int_{(k-1)/n}^{k/n} f(x)\d x-\frac{1}{n}\sum_{k=1}^nf\left(\frac{k}{n}\right)}\\
      &=\abs{\sum_{k=1}^n\int_{(k-1)/n}^{k/n} f(x)\d x-\sum_{k=1}^n\int_{(k-1)/n}^{k/n}f\left(\frac{k}{n}\right)\d x}\\
      &\leq\sum_{k=1}^n\int_{(k-1)/n}^{k/n}\abs{f(x)-f\left(\frac{k}{n}\right)}\d x.
    \end{align*}
  \item Soit $k\in\{1,\dots,n\}$ et $x\in\left[\frac{k-1}{n},\frac{k}{n}\right]$. Comme $f$ est de classe $\mc C^1$, le théorème des accroissements finis implique qu'il existe $x_0\in\left]x,\frac{k}{n}\right[$ tel que
    $$
    f(x)-f\left(\frac{k}{n}\right)=\left(x-\frac{k}{n}\right)f'(x_0).
    $$
    Or, $f'$ est continue sur un intervalle fermé, et y est donc bornée. Il existe $M>0$ tel que $\forall y\in\left[0,1\right],\,\abs{f'(y)}\leq M$. Ainsi, il vient
    $$
    \abs{f(x)-f\left(\frac{k}{n}\right)}=\abs{x-\frac{k}{n}}\abs{f'(x_0)}\leq M\abs{x-\frac{k}{n}} \leq \frac{M}{n}.
    $$
  \item On déduit de l'inégalité précédente que
    $$
    \int_{(k-1)/n}^{k/n}\abs{f(x)-f\left(\frac{k}{n}\right)}\d x \leq \int_{(k-1)/n}^{k/n}\frac{M}{n}\d x=\frac{M}{n^2}.
    $$
    En sommant sur $k\in\{1,\dots,n\}$, on en déduit que $\abs{S-S_n}\leq\frac{M}{n}$. En particulier, cela montre que $\lim_{n\to+\infty}S_n=S$, résultat dont on se sert dans les exercices précédents.
  \end{enumerate}
\end{cor}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
