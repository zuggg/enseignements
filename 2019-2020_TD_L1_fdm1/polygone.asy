if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

import patterns;

unitsize(2cm);

// le script trace un polygone régulier à 2n côtés
int n=5;
real theta=180/n;

// z=a+ib est l'affixe du point M
real a=0.4;
real b=0.15;

pen p = linewidth(1bp)+black;

// axes
real xs=1.5;
real ys=1.2;
draw ( (-xs,0) -- (xs,0), arrow=Arrow(TeXHead) );
draw ( (0,-ys) -- (0,ys), arrow=Arrow(TeXHead) );

draw(unitcircle);

// draw(rotate(theta/2)*polygon(2*n),pen=(linewidth(1bp)+black));
draw(polygon(2*n), p);

// colorier un triangle sur 2


add("hatch",hatch(1mm,NNE));


for (int i=0; i<=n-1; ++i)
  filldraw( (a,b) -- rotate(2*i*theta)*(1,0) -- rotate((2*i+1)*theta)*(1,0) -- cycle , pattern("hatch"),p );


// ajouter les labels aux sommets
label("$M$",(a,b),align=SW,filltype=Fill(white));
dot((a,b), linewidth(4bp));

for (int i=0; i<=2*n-1; ++i)
  label("$A_{"+string(i)+"}$", rotate(i*theta)*(1,0) , align=rotate(i*theta)*E,filltype=Fill(white));


