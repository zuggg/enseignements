\documentclass[11pt,a4paper,oneside]{article}
\usepackage{amsfonts,amsmath,amssymb,theorem,graphicx}
\usepackage[french]{babel}
%\usepackage[latin1]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{multienum,enumerate}
\usepackage{multicol}
\usepackage{tasks}
%\pagestyle{empty}

%---- Dimensions des marges ---
\setlength{\paperwidth}{21cm}
\setlength{\paperheight}{29.7cm}
\setlength{\evensidemargin}{-0.7cm}
\setlength{\oddsidemargin}{-0.7cm}
\setlength{\topmargin}{-2.5cm}
\setlength{\headsep}{0.7cm}
\setlength{\headheight}{1cm}
\setlength{\textheight}{25.5cm}
\setlength{\textwidth}{17.3cm}


%---- Ensembles : entiers, reels, complexes... ----
\newcommand{\N}{\textbf{N}}
\newcommand{\Z}{\textbf{Z}}
\newcommand{\Q}{\textbf{Q}}
\newcommand{\R}{\textbf{R}}
\newcommand{\C}{\textbf{C}}
\newcommand{\K}{\textbf{K}}
\newcommand{\intg}{[\![}
\newcommand{\intd}{]\!]}

%---- Modifications de symboles -----
\newcommand{\re}{\mathop{\mathrm{Re}}\nolimits}
\newcommand{\im}{\mathop{\mathrm{Im}}\nolimits}
\renewcommand{\P}{\mathcal{P}}
\newcommand{\F}{\mathcal{F}}

%---- Style de l'entete -----
\setlength{\parindent}{0cm}
\newcommand{\entete}
{
% Lieu - annee
  \noindent {\textbf{Université Claude Bernard - Lyon 1} \hfill Semestre automne 2019--2020}\\
  %  Module
  \noindent {Fondamentaux des math\'ematiques I\\}
  % Titre
  \hrule
  \begin{center} \textbf{Feuille d'exercices \no7b} \\ \textsc{Nombres complexes (deuxième partie : trigonométrie)} \end{center}
  \hrule
  \vspace*{1cm}
}

%--------- Exercice--------------
\newcounter{exercice}\setcounter{exercice}{1}
 \newenvironment{exo}[1][]
 {\pagebreak[3]                           \bigskip                           {\noindent  {\bf \underline{Exercice \theexercice.}} #1}                         }{\stepcounter{exercice} \medskip}



 \begin{document}
\baselineskip=0.6cm
\entete

{\bf 3. Forme trigonom\'etrique, argument}
\vskip2mm

\begin{exo} 
  Écrire sous forme trigonométrique les nombres suivants :
  \begin{tasks}(3)
    \task $i$
    \task $1+i$
    \task $1 - i$
    \task $\sqrt 3 + i$
    \task $-1$
    \task 1
  \end{tasks}
\end{exo}


\begin{exo} \begin{enumerate}
\item Calculer le module et un argument de $\dfrac{1+i\sqrt{3}}{\sqrt{3}+i}$.
\item \'Ecrire sous forme trigonométrique $\displaystyle\left( \frac{1+i\sqrt{3}}{1-i} \right)^4$.
\end{enumerate}
\end{exo}

\begin{exo} Soient  $\theta \in \R$ et $z = e^{i\theta}$. D\'eterminer la forme trigonom\'etrique de $1+z+z^2$.
\end{exo}

\begin{exo}
  D\'eterminer tous les entiers $n \in \N$ tels que
  \begin{tasks}(2)
    \task \((1+i)^n \in \R\)
    \task \((\sqrt{3}+i)^n \in i\R\)
  \end{tasks}
\end{exo}


\begin{exo}
\begin{enumerate}
 \item Soient $\theta \in ]-\pi, \pi[$ et $t = \tan\left(\frac{\theta}{2}\right)$. D\'emontrer l'identit\'e $$e^{i\theta} = \frac{1+it}{1-it},$$  puis exprimer $\cos \theta$ et $\sin \theta$ en fonction de $t$.
 
 \item En d\'eduire, pour tout $x \in {\bf R}$, une simpification de $\cos(2\arctan x)$ et $\sin(2\arctan x)$.
\item D\'emontrer que, pour tout $z \in \C \setminus \R_-$, $$\arg (z) \equiv 2 {\rm arctan} \left(\frac{{\rm Im}(z)}{{\rm Re}(z) + |z|}\right) \ \ \ ({\rm mod} \ 2\pi)$$
\end{enumerate}

\end{exo}


\vskip4mm
{\bf 4. Racines de l'unité}
\vskip2mm

\begin{exo}
Résoudre en $z\in\C$ les équations suivantes :
\begin{tasks}(3)
\task $z^3=-8i$
\task $z^5-z=0$ 
\task $27(z-1)^6+(z+1)^6=0$
\task $z^2\bar{z}^7=1$ 
\task $z^6-(3+2i)z^3+2+2i=0$
\end{tasks}
\end{exo}


\begin{exo}
Soit $n\in\N^*$.
\begin{enumerate}
\item Calculer la somme des racines $n$-ièmes de l'unité.
\item Calculer le produit des racines $n$-ièmes de l'unité.
\item On pose $\omega=e^{\frac{2i\pi}{n}}$. Calculer $\displaystyle\sum_{k=0}^{n-1} (1+\omega^k)^n$. \\(\emph{Indication : on pourra commencer par calculer $\sum_{k = 0}^{n-1} (\omega^j)^k$ pour tout $j \in \{0,\ldots, n\}$})
\end{enumerate}
\end{exo}


\begin{exo}
Soit $z$ un nombre complexe. Prouver les identités suivantes :

$$\sum_{k=0}^{18}\left(z-e^{2ik\pi/19}\right)^2=19z^2  \ \ \ {\rm et} \ \ \ \sum_{k=0}^{18}\left|z-e^{2ik\pi/19}\right|^2=19(1+|z|^2).$$
\end{exo}

\vskip4mm
{\bf 5. Angles remarquables}
\vskip2mm

\begin{exo}
On note $z_1=\dfrac{\sqrt{6}+i\sqrt{2}}{2}$ et $z_2=1+i$ puis on définit $z_3=\dfrac{z_1}{z_2}$.
\begin{enumerate}
\item \'Ecrire $z_1$, $z_2$ et $z_3$ sous forme trigonométrique.
\item En déduire des expressions de $\cos \frac{7 \pi}{12}$ et $\sin \frac{7 \pi}{12}$.
\end{enumerate}
\end{exo}

\begin{exo}
\begin{enumerate}
\item Résoudre algébriquement l'équation $z^2=1+i$, d'inconnue $z\in\C$.
\item En déduire des expressions de $\cos\frac{\pi}{8}$ et $\sin \frac{\pi}{8}$.
\end{enumerate}
\end{exo}

\begin{exo}
On note $\omega=e^{2i\pi/5}$.
\begin{enumerate}
\item Quelle relation simple lie les nombres $\cos(\frac{2\pi}5)$ et $\omega +\frac 1\omega$ ?
\item Justifier l'identité $\left(\omega+\frac1\omega\right)^2+\left(\omega+\frac1\omega\right)-1=0$.
\item Calculer $\cos(\frac{2\pi}5)$.
\end{enumerate}
\end{exo}

\vskip4mm
{\bf 6. D'autres applications à la trigonométrie}
\vskip2mm

\begin{exo} \textit{Réduction de $a\cos x+b\sin x$}.
\begin{enumerate}
\item Soient $a$ et $b$ deux réels. Démontrer qu'il existe $r\in\R_+$ et $\theta\in\R$ tels que
$$
\forall x\in\R,\;a\cos x+b\sin x=r\cos(x+\theta).
$$
\item Déterminer l'ensemble des $x\in \R$ qui vérifient $\cos x+\sin x=1$.
\end{enumerate}
\end{exo}

\begin{exo}
Développer $\cos(2\varphi)$ pour obtenir un polynôme en \(\cos(\varphi)\), puis $\sin(3\varphi)$ pour obtenir un polynôme en \(\sin(\varphi)\).
\end{exo}

\begin{exo}
Pour tout $n \in \N$ et tout $\theta \in \R$, calculer
$$
\sum_{k=0}^n \cos(k\theta)
\quad\textrm{et}\quad
\sum_{k=0}^n \sin(k\theta).
$$
\end{exo}

\end{document}

%% \begin{exo}
%% Décrire les ensembles suivants :
%% \begin{enumerate}
%% \item $\left\{\ z\in\C\ \middle|\ z^2+2z-3 \in \R\ \right\}$;
%% %\item $\left\{\ z\in\C\ \middle|\ |1-z|\leq\dfrac{1}{2}\ \right\}$ ;
%% %\item $\left\{\ z\in\C\ \middle|\ \re (1-z)\leq\dfrac{1}{2}\ \right\}$;
%% \item $\left\{\ z\in\C\setminus\{-3\}\ \middle|\ \left|\dfrac{z-3}{z+3}\right| <2\ \right\}$;
%% %\item $\left\{\ z\in\C\ \middle|\ \re (iz)\leq\dfrac{1}{2}\ \right\}$;
%% \item $\left\{\ z\in\C^\star\ \middle|\ \left|1-\dfrac{1}{z}\right|^2=2\ \right\}$.
%% \end{enumerate}
%% \end{exo}




%% \begin{exo}
%% \begin{enumerate}
%% \item Soit $a$ et $b$ deux nombres complexes.
%% \begin{enumerate}
%% \item Montrer que $\re (\bar{a}b)=\re\left(\bar{b}a\right)$.
%% \item Exprimer $\re (\bar{a}b)$ à l'aide de formes trigonométriques de $a$ et $b$, puis à l'aide des formes algébriques de $a$ et $b$.
%% \item Montrer que $|\re (\bar{a}b)|\ \leq\ |a||b|.$
%% \item Déterminer le cas d'égalité dans l'inégalité précédente et l'exprimer à l'aide de formes trigonométriques de $a$ et $b$.
%% \item Montrer que
%% $$
%% |a+b|^2\ =\ |a|^2+2\re(\bar{a}b)+|b|^2\ .
%% $$
%% \item Démontrer l'identité du parallélogramme
%% $$
%% |a+b|^2\ +\ |a-b|^2\ =\ 2\,(|a|^2+|b|^2)\ .
%% $$
%% \end{enumerate}
%% \item Montrer que, pour tout $(a,b,z)\in\C^3$, on a:
%% $$
%% |z-a|^2\leq|z-b|^2
%% \quad\textrm{si et seulement si}\quad
%% \re\left(\overline{\left(z-\frac{a+b}{2}\right)}\,\left(b-a\right)\right)\leq0\ .
%% $$
%% \end{enumerate}
%% \end{exo}

%% \begin{exo}
%% On rappelle l'identification canonique de $\R^2$ et de $\C$ par l'application affixe et sa réciproque:
%% $$
%% \begin{array}{rcl}\R^2&\to&\C\\(x,y)&\mapsto&x+iy\end{array}
%% \qquad\textrm{et}\qquad
%% \begin{array}{rcl}\C&\to&\R^2\\z&\mapsto&(\re z,\im z)\end{array}\ .
%% $$
%% \begin{enumerate}
%% \item Rappeler l'effet sur $\C$ des transformations du plan suivantes :
%% \begin{enumerate}
%% \item pour $a\in\C$, la translation du vecteur d'affixe $a$ ;
%% \item pour $(a,\lambda)\in\C\times\R$, l'homothétie de rapport $\lambda$ et de centre d'affixe $a$;
%% \item pour $(a,\theta)\in\C\times\R$, la rotation d'angle $\theta$ et de centre d'affixe $a$;
%% \item pour $(a,\theta)\in\C\times\R$, la symétrie par rapport à un axe formant un angle $\theta$ avec l'axe réel et passant par un point d'affixe $a$.
%% \end{enumerate}
%% \item Déterminer la nature des similitudes correspondant aux applications suivantes.
%% \begin{enumerate}
%% \item $\varphi_1:\C\to\C,\ z\mapsto \left(\frac12+i\frac{\sqrt{3}}{2}\right)z+3$.
%% \item $\varphi_2:\C\to\C,\ z\mapsto i\,\bar z$.
%% \end{enumerate}
%% \item Montrer que la composée de deux symétries est une translation ou une rotation.
%% \item Montrer que la composée de deux rotations est une translation ou une rotation.
%% \end{enumerate}
%% \end{exo}

%% \begin{exo}
%% On note $A$ le point d'affixe $4+2i$ et $O$ celui d'affixe $0$.\\
%% Calculer les affixes des points $B$ tels que le triangle $OAB$ soit équilatéral.
%% \end{exo}

%% %\begin{exo}
%% %Soit $\mathcal{D}_1$ et $\mathcal{D}_2$ deux droites du plan non parallèles.\\
%% %Montrer qu'elles s'intersectent en un seul point.
%% %\end{exo}

%% \begin{exo}
%% Déterminer l'ensemble des $z\in\C$ tels que soient alignés les points d'affixes $z$, $iz$ et $i$.
%% \end{exo}

%% \begin{exo}
%% \begin{enumerate}
%% \item Pour tout $n\in\N$, exprimer, pour tout $\theta\in\R$, $\cos(n\theta)$ et $\sin(n\theta)$ en fonction de $\cos(\theta)$ et $\sin(\theta)$. Indication : utiliser la formule du binôme de Newton.
%% \item Pour tout $n\in\N$ impair, exprimer, pour tout $\theta\in\R$, $\cos(n\theta)$ en fonction de $\cos(\theta)$ et $\sin(n\theta)$ en fonction de $\sin(\theta)$.
%% \item Soit $n\in\N^\star$ pair. Exprimer, pour tout $\theta\in\R$, $\cos(n\theta)$ en fonction de $\cos(\theta)$, et faire de même en fonction de $\sin(\theta)$.
%% \end{enumerate}
%% \end{exo}


